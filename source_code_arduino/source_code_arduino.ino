#define IN1 4
#define IN2 5
#define IN3 6
#define IN4 7
#define ENA 3
#define ENB 11

#define R_IR_2 A4
#define R_IR_1 A3
#define M_IR A2
#define L_IR_1 A1
#define L_IR_2 A0

#define STATE HIGH
#define LOOP_ENGINE 300

int lState1, lState2, mState, rState1, rState2;

void read_irs();
void go();
void turn(int, bool);
void goAhead();
void startEngine(int, int);
int calculate_error();
void go();

void setup() {
  pinMode(L_IR_1, INPUT);
  pinMode(L_IR_2, INPUT);
  pinMode(M_IR, INPUT);
  pinMode(R_IR_1, INPUT);
  pinMode(R_IR_2, INPUT);
  
  pinMode(ENA, OUTPUT);
  pinMode(ENB, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);

  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
}

void loop() {
  read_irs();
  go();
}

void read_irs() {
  lState1 = digitalRead(L_IR_1);
  lState2 = digitalRead(L_IR_2);
  mState = digitalRead(M_IR);
  rState1 = digitalRead(R_IR_1);
  rState2 = digitalRead(R_IR_2);
}

int calculate_error() {
  static int error = 0;
  if (mState == STATE) {
    if ((mState == lState1) && (mState != rState1)) { // 01100
      error = 1;
    } else if ((mState != lState1) && (mState == rState1)) { // 00110
      error = -1;
    } else { // 00100
      error = 0;
    }
  } else if (lState1 == STATE) {
    if (lState1 != lState2) { // 01000
      error = 2;
    } else { // 11000
      error = 3;
    }
  } else if (lState2 == STATE) { // 10000 
    error = 4;
  } else if (rState1 == STATE) {
    if (rState1 != rState2) { // 00010
      error = -2;
    } else { // 00011
      error = -3;
    }
  } else if (rState2 == STATE) { // 00001
    error = -4;
  } else {
    error = 0;
  }
  return error;
}

void go() {
  int error = calculate_error();
  if (error == 0) {
    goAhead();
  } else if (error > 0) {
    turn(error, true); // turn left
  } else {
    turn(-error, false); // turn right
  }
}
void goAhead(){
    startEngine(2, 2);
}

void turn(int level, bool toLeft){
  int val = 0;
  switch (level){
    case 1:
      val = 6;
      break;
    case 2:
      val = 8;
      break;
    case 3:
      val = 10;
      break;
    case 4:
      val = 12;
      break;
    default:
      val = 0;
  }
  return toLeft ? startEngine(0, val) : startEngine(val, 0);
}

void startEngine(int aSpeed, int bSpeed) {
  static int _aSpeed, _bSpeed;
  for (int i = 0; i < LOOP_ENGINE; i++) {
    if (i%3 == 0) {
      _aSpeed = 90 + aSpeed*3;
      _bSpeed = 90 + bSpeed*3;
    } else {
      _aSpeed = aSpeed;
      _bSpeed = bSpeed;
    }
    analogWrite(ENA, _aSpeed);
    analogWrite(ENB, _bSpeed);
  }
}
